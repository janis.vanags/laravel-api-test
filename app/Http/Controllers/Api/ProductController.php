<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Http\Resources\ProductResource;

class ProductController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->jsonResponse(
            ProductResource::collection(Product::all())
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->jsonError('Validation error.', $validator->errors(), 401);
        }

        $product = Product::create($input);

        $product->attributes()->createMany($input['attributes']);

        return $this->jsonResponse(new ProductResource($product));
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        return $this->jsonResponse(new ProductResource($product));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->jsonError('Validation error.', $validator->errors());
        }

        $product->name = $input['name'];
        $product->description = $input['description'];
        $product->save();

        collect($input['attributes'])->each(function (array $row) use ($product) {
            $product->attributes()->updateOrCreate(
                ['key' => $row['key']],
                ['value' => $row['value']],
            );
        });

        return $this->jsonResponse(new ProductResource($product));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return $this->jsonResponse([]);
    }
}
