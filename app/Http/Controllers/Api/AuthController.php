<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AuthController extends BaseController
{

    /**
     * Register.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()){
            return $this->jsonError('Validation error.', $validator->errors(), 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        return $this->jsonResponse([
            'token' => $user->createToken('api')->plainTextToken,
            'name' => $user->name
        ]);
    }

    /**
     * Login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()){
            return $this->jsonError('Validation error.', $validator->errors(), 401);
        }

        if (!Auth::attempt($request->only(['email', 'password']))) {
            return $this->jsonError('Login failed');
        }

        $user = Auth::user();

        return $this->jsonResponse([
            'token' => $user->createToken('api')->plainTextToken,
            'name' => $user->name
        ]);
    }
}
