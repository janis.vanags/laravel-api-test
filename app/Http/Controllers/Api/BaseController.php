<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{

    /**
     * Success response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse($data = [])
    {
        $response = [
            'success' => true,
            'data'    => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Error response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($error, $data = [], $code = 500)
    {
        $response = [
            'success' => false,
            'error' => $error,
            'data' => $data
        ];

        return response()->json($response, $code);
    }
}
