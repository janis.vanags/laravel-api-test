#!/bin/bash

if [ $# -eq 0 ]; then
    echo "usage: ./dev.sh COMMAND

    Commands:
    up - build, run & initialize project for the first time
    down - stops and removes containers and networks created by up
    down all - stops and removes containers, networks, images and volumes created by up (including all database data)
    build - rebuild project container
    rebuild - rebuild project container without using docker cache
    refresh - rerun initialization commands
    start - start containers
    stop - stop containers
    logs - show & follow all container logs (CTRL+C to exit)
    "
    exit 0
fi

COMPOSEFILE=.env-config/docker-compose.dev.yml
SERVICENAME=website
COMPOSECMD="docker-compose -f $COMPOSEFILE exec -T $SERVICENAME bash -c"
# https://docs.docker.com/compose/reference/
# https://docs.docker.com/engine/reference/commandline/compose_build/

initProject () {
    # maintenance mode on
    $COMPOSECMD "php artisan down"

    # permissions
    $COMPOSECMD "chown -R application:application /app/storage"
    $COMPOSECMD "chmod -R ugo+w /app/bootstrap/cache"

    # db migrations
    $COMPOSECMD "php artisan migrate"

    # clear caches
    $COMPOSECMD "rm -rf /app/storage/framework/cache/data/*"
    $COMPOSECMD "php artisan cache:clear"
    $COMPOSECMD "php artisan view:clear"
    $COMPOSECMD "php artisan view:cache"
    $COMPOSECMD "php artisan route:clear"
    $COMPOSECMD "php artisan route:cache"
    # config:cache should only be used if config() is used correctly in the code, see:
    # https://stackoverflow.com/questions/40026893/what-is-difference-between-use-envapp-env-configapp-env-or-appenviron
#    $COMPOSECMD "php artisan config:clear"
#    $COMPOSECMD "php artisan config:cache"

    # finalize
    $COMPOSECMD "php artisan storage:link"
    $COMPOSECMD "php artisan vendor:publish --all"
    $COMPOSECMD "php artisan optimize"

    # maintenance mode off
    $COMPOSECMD "php artisan up"
}

if [ "$1" = "up" ]; then

    # build & run all containers detached
    docker-compose -f $COMPOSEFILE up -d &&

    # going to sleep so database has time to initialize
    sleep 10 &&

    initProject

elif [ "$1" = "down" ]; then

    if [ "$2" = "all" ]; then
        docker-compose -f $COMPOSEFILE down --rmi all --volumes
    else
        docker-compose -f $COMPOSEFILE down
    fi

elif [ "$1" = "build" ]; then

    docker-compose -f $COMPOSEFILE up --build --force-recreate --no-deps -d $SERVICENAME && initProject

elif [ "$1" = "rebuild" ]; then

    docker-compose -f $COMPOSEFILE build --no-cache $SERVICENAME
    docker-compose -f $COMPOSEFILE up --force-recreate --no-deps -d $SERVICENAME && initProject

elif [ "$1" = "refresh" ]; then

    initProject

elif [ "$1" = "start" ]; then

    docker-compose -f $COMPOSEFILE start

elif [ "$1" = "stop" ]; then

    docker-compose -f $COMPOSEFILE stop

elif [ "$1" = "logs" ]; then

    docker-compose -f $COMPOSEFILE logs -f --tail 500

fi

exit 0
